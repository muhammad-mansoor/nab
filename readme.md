# NAB Base Template

## Desktop
- Develop the HTML/CSS in source.
- If mobile is not required. Delete the source/mobile folder.

## Mobile
- Develop the HTML/CSS in source/mobile.
- Ensure redirect is setup correctly.