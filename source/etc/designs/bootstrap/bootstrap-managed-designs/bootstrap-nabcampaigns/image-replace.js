$(document).ready(function() {
	var retina = window.devicePixelRatio >1 ? true : false;
	if(retina){
		imageReplace();
	}
});

function imageReplace() {
	// masthead / For future retina images add class="highres"
	$("#banner div.slideshow-masthead div img, img.highres").each(function () {
		var src = $(this).attr("src");
		$(this).attr("src", src.replace(/\.(png|PNG|jpg|JPG|gif|GIF|jpeg|JPEG)$/, "_2x.$1"));
	});

	//SOCIAL ICONS: standard icons are dynamic in wcm, retina icons are stored on webserver
	var social=["facebook", "twitter", "youtube", "googleplus", "linkedin"];
	for (i=0;i<social.length;i++){
		$("ul#social li."+social[i]+" img").attr("src", "/includes/nab/files/images/social-"+social[i]+"_2x.png");
	}
}