// Responsive table
/*
function tableResponsive(){
    var e = $('.campaign-table tbody tr td:first-child');
    if ($(window).width() < 768) {
        
        e.addClass('title');
        e.nextAll('td').addClass('invisible');
        e.on('click', function(){
            
            if ($(this).hasClass('rotate')){
                $(this).removeClass('rotate').nextAll('td').addClass('invisible');
            }
            else {
                
                $(this).addClass('rotate').nextAll('td').removeClass('invisible');
                $(this).parent().siblings().find('td:first-child').removeClass('rotate').nextAll('td').addClass('invisible');
             // e.parent().nextAll("td").hide();
            }
        });
    } else {
        e.removeClass('title');
        e.nextAll('td').removeClass('invisible');
    }
}
*/

$(document).ready(function() {
    $('.accordion-section-title').on('click',function(e) {
        // Grab current anchor value

        var clickTarget = $(this);

        var currentAttrValue = clickTarget.attr('href');
        if(clickTarget.is('.active')) {
            clickTarget.removeClass('active');
            $('.accordion .accordion-section-content' + currentAttrValue).slideUp(300).removeClass('open');
            clickTarget.find(".icon-expand").removeClass('rotate');

        }else {
            // Add active class to section title
            clickTarget.addClass('active');
            // Open up the hidden content panel
            $('.accordion ' + currentAttrValue).slideDown(300).addClass('open');
            clickTarget.find(".icon-expand").addClass('rotate');
        }

        e.preventDefault();

    });

    $('.nav-dropdown-toggle').on('click',function(){
        if( $('.nav-collapse').hasClass('hidden-md-under') ){
            $('.nav-collapse').removeClass('hidden-md-under');
            $('.icon-arrow').removeClass('icon-arrow-down').addClass('icon-arrow-up');
        }else{
            $('.nav-collapse').addClass('hidden-md-under');
            $('.icon-arrow').removeClass('icon-arrow-up').addClass('icon-arrow-down');
        }

    });

    $(window).bind('scroll', function(){
        if( ( $(this).scrollTop() > 700 ) && ( $(this).width() > 800 ) ) {
            $('.get-started-bar').addClass('fix-position');
        }
        else {
            $('.get-started-bar').removeClass('fix-position');   
        }

        if( ( $(this).scrollTop() >= 1800 ) && ( $(this).width() > 800 ) ) {
            $('.get-started-bar').hide();
        }
         else {
            $('.get-started-bar').show();   
        }
        
    });

    //tableResponsive();
    
});

$(window).on('resize', function() {
   // tableResponsive();
});