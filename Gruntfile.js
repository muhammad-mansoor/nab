module.exports = function(grunt) {

  // Project configuration.
  grunt.initConfig({
    
    pkg: grunt.file.readJSON('package.json'),

    paths: {
      src: 'source/',
      build: 'build/',
      temp: 'temp/'
    },

    clean: {
      desktop: {
        src: [ '<%= paths.build %>' ]
      },
      temp: {
        src: [ '<%= paths.temp %>' ]
      }
    },

    concat: {
      options: {
          stripBanners: false,
          separator: ';'
      },
      vendor: {
          src: [
            '<%= paths.src %>assets/js/vendor/*.js'
          ],
          dest: '<%= paths.temp %>assets/js/vendor.js'
      },
      modules: {
        src: [
          '<%= paths.src %>assets/js/modules/*.js'
        ],
        dest: '<%= paths.temp %>assets/js/modules.js'
      },
      script_footer: {
          src: [
            '<%= concat.vendor.dest %>',
            '<%= concat.modules.dest %>',
            '<%= paths.src %>assets/js/global.js'
          ],
          dest: '<%= paths.temp %>assets/js/script-foot.js'
      },
      dev: {
        src: '<%= paths.temp %>assets/js/script-foot.js',
        dest: '<%= paths.build %>assets/js/script-foot.min.js'
      }
    },

    uglify: {
      options: {
        banner: '/*! <%= pkg.name %> <%= pkg.version %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      build: {
        options: {
          compress: {
            // drop_console: true
          }
        },
        files: {
          '<%= paths.build %>assets/js/script-foot.min.js': ['<%= paths.temp %>assets/js/script-foot.js']
        }
      },
    },

    jshint: {
      options: {
        curly: true,
        eqnull: true,
        browser: true,
        globals: {
          jQuery: true
        },
      },
      gruntfile: {
          src: 'Gruntfile.js'
      },
      src: {
          src: [
            '<%= paths.src %>assets/js/modules/*.js',
            '<%= paths.src %>assets/js/main.js'
          ]
      }
    },

    less: {
      options: {
        compile: true,
        banner: '/*! <%= pkg.name %> <%= pkg.version %> <%= grunt.template.today("yyyy-mm-dd") %> */\n'
      },
      dev_desktop: {
        options: {
          compress: false,
          compile: true,
          dumpLineNumbers: 'all'
        },
        files: { '<%= paths.build %>assets/css/custom.css': ['<%= paths.src %>assets/less/_custom.less'] }
      },
      desktop: {
        options: {
          compress: true,
          compile: true
        },
        files: { '<%= paths.build %>assets/css/custom.css': ['<%= paths.src %>assets/less/_custom.less'] }
      },
      styleguide: {
        options: {
          compress: true,
          compile: true
        },
        files: { '<%= paths.build %>assets/css/styleguide.css': ['<%= paths.src %>assets/less/_styleguide.less'] }
      }
    },

    accessibility: {
      options : {
        accessibilityLevel: 'WCAG2AA',
        reportType: 'csv',
        reportLocation : '<%= paths.src %>/accessibility/',
        ignore : [
          'WCAG2AA.Principle2.Guideline2_4.2_4_2.H25.2',
          'WCAG2AA.Principle1.Guideline1_4.1_4_3.G18.Fail',
          'WCAG2AA.Principle2.Guideline2_4.2_4_1.G1,G123,G124.NoSuchID',
          'WCAG2AA.Principle2.Guideline2_4.2_4_4.H77,H78,H79,H80,H81',
          'WCAG2AA.Principle3.Guideline3_1.3_1_1.H57.2'
        ]
      },
      test : {
        src: ['<%= paths.src %>/*.html']
      }
    },

    copy: {
      desktop: {
        expand: true,
        cwd: '<%= paths.src %>',
        src: [
          'assets/img/{,**/}*',
          'assets/fonts/{,**/}*',
          'etc/{,**/}*',
          '*.ico',
          '*.html'
        ],
        dest: '<%= paths.build %>'
      }
    },

    assemble: {
      // Task-level options
      options: {
        prettify: {indent: 2},
        marked: {sanitize: false},
        production: false,
        assets: '<%= paths.src %>',
        layoutdir: '<%= paths.src %>templates/layouts',
        partials: ['<%= paths.src %>templates/partials/**/*.hbs']
      },
      site: {
        // Target-level options
        options: {
          ext: '.html',
          layoutdir: '<%= paths.src %>templates/layouts',
          partials: ['<%= paths.src %>templates/partials/**/*.hbs']
        },
        files: [
          { expand: true, cwd: '<%= paths.src %>templates/pages', src: ['*.hbs'], dest: '<%= paths.build %>' }
        ]
      },
      siteStyleguide: {
        options: {
          ext: '.html',
          partials: ['<%= paths.src %>templates/styleguide/**/*.hbs']
        },
        files: [
          { expand: true, cwd: '<%= paths.src %>templates/styleguide', src: ['*.hbs'], dest: '<%= paths.build %>styleguide/' }
        ]
      },
      sitePartials: {
        // Target-level options
        options: {
          ext: '.html',
          layout: 'partial.hbs',
          partials: ['<%= paths.src %>templates/partials/**/*.hbs']
        },
        files: [
          { expand: true, cwd: '<%= paths.src %>templates/partials', src: ['*.hbs'], dest: '<%= paths.build %>partials/' }
        ]
      }
    },

    watch: {
      js: {
        options: {
          livereload: true
        },
        files: '<%= paths.src %>assets/js/{,**/}*.js',
        tasks: ['jshint', 'concat']
      },
      less: {
        options: {
          livereload: true
        },
        files: '<%= paths.src %>assets/less/{,**/}*.less',
        tasks: ['less:dev_desktop','less:styleguide']
      },
      html: {
        options: {
          livereload: true
        },
        files: '<%= paths.src %>{,**/}*.html',
        tasks: ['copy:desktop']
      },
      site: {
        files: ['<%= paths.src %>templates/layouts/**/*.hbs', '<%= paths.src %>templates/pages/**/*.hbs', '<%= paths.src %>templates/partials/**/*.hbs'],
        tasks: ['assemble:site']
      },
      sitePartials: {
        files: ['<%= paths.src %>templates/partials/**/*.hbs'],
        tasks: ['assemble:sitePartials']
      },
      siteStyleguide: {
        files: ['<%= paths.src %>templates/styleguide/**/*.hbs'],
        tasks: ['assemble:siteStyleguide']
      }
    },

    connect: {
      server: {
        options: {
          port: 9009,
          base: 'build'
        }
      }
    }

  });

  // Load the plugin that provides the "uglify" task.
  // Before loading npm run the following tasks:
  //    npm install

  grunt.loadNpmTasks('grunt-contrib-concat');
  grunt.loadNpmTasks('grunt-contrib-clean');
  grunt.loadNpmTasks('grunt-contrib-copy');
  grunt.loadNpmTasks('grunt-contrib-uglify');
  grunt.loadNpmTasks('grunt-contrib-jshint');
  grunt.loadNpmTasks('grunt-accessibility');
  grunt.loadNpmTasks('grunt-contrib-watch'); //run this task with "grunt watch"
  grunt.loadNpmTasks('grunt-contrib-less');
  grunt.loadNpmTasks('grunt-html-validation');
  grunt.loadNpmTasks('assemble');
  grunt.loadNpmTasks('grunt-contrib-connect');

  // Deploy assets to Desktop Only
  grunt.registerTask('desktop', ['clean:desktop', 'assemble:site', 'concat', 'copy:desktop', 'less:desktop', 'accessibility', 'uglify:build', 'clean:temp', 'connect:server', 'watch']);
  grunt.registerTask('styleguide', ['clean:desktop', 'assemble:site', 'assemble:siteStyleguide', 'assemble:sitePartials', 'concat', 'copy:desktop', 'less:desktop', 'less:styleguide', 'accessibility', 'uglify:build', 'clean:temp', 'connect:server', 'watch']);

  // Default task(s): Set here: if Mobile, Desktop must be done first.
  grunt.registerTask('default', ['desktop']);

};